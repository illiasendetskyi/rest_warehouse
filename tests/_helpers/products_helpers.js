const mongoose = require('mongoose');
const {Product} = require('./../../server/models/products');
const {Category} = require('./../../server/models/categories');
const randomstring = require('randomstring');
const categoriesForProducts = [{
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(5)
}, {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(5),
    description: randomstring.generate(7)
}, {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(5)
}];
const products = [{
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(6),
    categoryId: categoriesForProducts[0]._id,
    description: randomstring.generate(8),
    cost: randomstring.generate({
        length: 4,
        charset: 'numeric'
    })
}, {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(6),
    categoryId: categoriesForProducts[0]._id,
    cost: randomstring.generate({
        length: 4,
        charset: 'numeric'
    })
}, {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(6),
    categoryId: categoriesForProducts[1]._id,
    cost: randomstring.generate({
        length: 4,
        charset: 'numeric'
    })
}];
const testProduct = {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(6),
    categoryId: categoriesForProducts[1]._id,
    cost: randomstring.generate({
        length: 4,
        charset: 'numeric'
    })
};
const addProducts = async () => {
    await Product.insertMany(products);
};
const delProducts = async () => {
    await Product.remove({});
};
const addCategoriesForProducts = async () => {
    await Category.insertMany(categoriesForProducts);
};
const delCategoriesForProducts = async () => {
    await Category.deleteMany({
        _id: {
            $in:[categoriesForProducts[0]._id,
                categoriesForProducts[1]._id,
                categoriesForProducts[2]._id]
        }
    });
};

module.exports = {
    addProducts, delProducts,
    products, testProduct, addCategoriesForProducts, delCategoriesForProducts, categoriesForProducts
};