const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('./../../config/config');
const {User} = require('./../../server/models/users');
const randomstring = require('randomstring');
const signupUser = {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(5),
    email: randomstring.generate(5) + '@email.com',
    password: randomstring.generate(6)
};
const testUser = {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(6),
    email: randomstring.generate(6) + '@email.com',
    password: randomstring.generate(6)
};
const validToken = jwt.sign({_id: signupUser._id}, JWT_SECRET);
const expiredToken = jwt.sign({_id: signupUser._id}, JWT_SECRET, {expiresIn: 0});
const signUp = async () => {
    await new User(signupUser).save();
};
const delUsers = async () => {
    await User.deleteMany({
        _id: {
            $in: [signupUser._id,
                testUser._id]
        }
    });
};

module.exports = {signUp, delUsers, signupUser, testUser, expiredToken, validToken};