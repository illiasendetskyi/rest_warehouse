const supertest = require('supertest');
const app = require('./../server/server');
const test = require('ava');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('./../config/config');
const request = supertest(app);
const {signUp, delUsers} = require('./_helpers/users_helpers');
const {signupUser, testUser, validToken, expiredToken} = require('./_helpers/users_helpers');
test.before(signUp);
test.after.always(delUsers);
test('\'POST /signup\' should sign up with valid data and return signuped user with hashed password', async t => {
    const res = await request
        .post('/signup')
        .send(testUser);
    t.is(res.status, 200);
    t.deepEqual(bcrypt.compareSync(testUser.password, res.body.password), true);
    t.deepEqual(res.body.name, testUser.name);

});

test('\'POST /login\', should login with valid data and get back response with valid JWT', async t => {
    const res = await request
        .post('/login')
        .send({
            email: signupUser.email,
            password: signupUser.password
        });
    t.is(res.status, 200);
    t.deepEqual(jwt.verify(res.body.token, JWT_SECRET)._id, signupUser._id.toHexString());
});
test('\'POST /me\', should send back the authenticated user if token is valid', async t => {
    const res = await request
        .post('/me')
        .set('Authorization', `Bearer ${validToken}`);
    t.is(res.status, 200);
    t.deepEqual(res.body._id, signupUser._id.toHexString());
});
test('\'POST /signup\', should not sign up with duplicate email', async t => {
    const res = await request
        .post('/signup')
        .send(signupUser);
    t.is(res.status, 400);
});
test('\'POST /signup\', should not sign up with invalid name', async t => {
    const res = await request
        .post('/signup')
        .send({
            name: '',
            email: 'example@email.com',
            password: 'abc123'
        });
    t.is(res.status, 400);
});
test('\'POST /signup\', should not sign up with invalid email', async t => {
    const res = await request
        .post('/signup')
        .send({
            name: 'name',
            email: 'invalid_email',
            password: 'abc123'
        });
    t.is(res.status, 400);
});
test('\'POST /signup\', should not sign up with too short password', async t => {
    const res = await request
        .post('/signup')
        .send({
            name: 'name',
            email: 'example@email.com',
            password: 'abc12'
        });
    t.is(res.status, 400);
});
test('\'POST /login\', should not login with invalid password', async t => {
    const res = await request
        .post('/login')
        .send({
            email: signupUser.email,
            password: 'invalid_password'
        });
    t.is(res.status, 401);
});
test('\'POST /login\', should not login with invalid email', async t => {
    const res = await request
        .post('/login')
        .send({
            email: 'invalid_email',
            password: signupUser.password
        });
    t.is(res.status, 401);
});
test('\'POST /me\', should return 401 if token is expired', async t => {
    const res = await request
        .post('/me')
        .set('Authorization', `Bearer ${expiredToken}`);
    t.is(res.status, 401);
});
test('\'POST /me\', should return 401 if token is invalid', async t => {
    const res = await request
        .post('/me')
        .set('Authorization', `Bearer invalidToken123`);
    t.is(res.status, 401);
});