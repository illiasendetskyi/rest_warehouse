const app = require('./server/server');
const {port} = require('./config/config');
app.listen(port, () => {
    console.log(`Start on port: ${port}`);
});

module.exports = app;