const env = process.env.NODE_ENV || 'development';
let mongodb_URI = process.env.MONGODB_URI || 'mongodb://localhost:27016/rest_warehouse';
const port = process.env.PORT || 3000;
const JWT_SECRET = process.env.JWT_SECRET ||'silfbsigbisgbs;aa;lf';
if(env === 'test') {
    mongodb_URI = 'mongodb://localhost:27016/rest_warehouse_test';
    console.log('env*******************', env);
}
module.exports = {port, mongodb_URI, env, JWT_SECRET};