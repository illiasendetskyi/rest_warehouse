const LocalStrategy = require('passport-local').Strategy;
const {User} = require('./../server/models/users');
const bcrypt = require('bcryptjs');
const loginStrategy = new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    session: false
}, async (email, password, done) => {

    try {
        const user = await User.findOne({email});
        if (!user) {
            return done(null, false);
        }
        const matchPass = await bcrypt.compare(password, user.password);
        if (!matchPass) {
            return done(null, false);
        }
        return done(null, user);
    } catch (e) {
        return done(e);
    }

});


module.exports = loginStrategy;