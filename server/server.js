const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const loginStrategy = require('../config/loginStrategy');
const authenticateStrategy = require('../config/authenticateStrategy');
const categoryRouter = require('./router/categoryRouter');
const productRouter = require('./router/productRouter');
const authRouter = require('./router/authRouter');
const mongoose = require('./mongoose');
const app = express();


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/', authRouter);
passport.use('login', loginStrategy);
passport.use('authenticate_jwt', authenticateStrategy);
app.use('/categories', categoryRouter);
app.use('/products', productRouter);

module.exports = app;
