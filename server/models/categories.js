const mongoose = require('mongoose');
const CategorySchema = new mongoose.Schema({
    name: {
        required: true,
        type: String,
        unique: true,
        minlength: 2,
        trim: true
    },
    description: {
        type: String,
        default: 'no description',
        trim: true,
        minlength: 2
    }
});
module.exports = {Category:  mongoose.model('Category', CategorySchema)};