const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const validator = require('validator');
const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: validator.isEmail,
            message: 'not valid email'
        },
        minlength: 1,
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim: true,
        minlength: 6
    }
});

UserSchema.pre('save', function (next) {
    let user = this;
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(user.password, salt, (err, hash) => {
            user.password = hash;
            next();
        });
    });
});
module.exports = {User: mongoose.model('User', UserSchema)};