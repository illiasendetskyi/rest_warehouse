const mongoose = require('mongoose');
const ProductSchema = new mongoose.Schema({
    name: {
        required: true,
        type: String,
        unique: true
    },
    categoryId: {
        required: true,
        type: mongoose.Schema.Types.ObjectId
    },
    properties: {
        type: String,
        default: 'no properties',
        trim: true,
        minlength: 2
    },
    cost: {
        required: true,
        type: Number,
        default: 0,
        trim: true
    }
});
module.exports = {Product: mongoose.model('Product', ProductSchema)};