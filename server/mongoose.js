const mongoose = require('mongoose');
const {mongodb_URI} = require('./../config/config');
mongoose.Promise = global.Promise;
mongoose.connect(mongodb_URI, {useNewUrlParser: true}).catch((e) => {
    console.log(e);
});
module.exports = mongoose;