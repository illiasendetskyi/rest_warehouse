const express = require('express');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const {JWT_SECRET} = require('./../../config/config');
const authRouter = express.Router();
const {User} = require('./../models/users');
authRouter.post('/signup', async (req, res) => {
    try {
        const user = new User(req.body);
        const savedUser = await user.save({validateBeforeSave: true});
        res.json(savedUser);
    } catch (e) {
        res.status(400).send(e);
    }
});

authRouter.post('/login', passport.authenticate('login', {session: false}), async (req, res) => {
    try {
        const token = await jwt.sign({_id: req.user._id}, JWT_SECRET);
        res.json({token});
    } catch (e) {
        res.status(401).send(e);
    }
});

authRouter.post('/me', passport.authenticate('authenticate_jwt', {session: false}),
    (req, res) => {
        res.send(req.user);
    }
);

module.exports = authRouter;



