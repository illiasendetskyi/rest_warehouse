const express = require('express');
const _ = require('lodash');
const passport = require('passport');
const {Product} = require('./../models/products');
const {Category} = require('./../models/categories');
const productRouter = express.Router();

productRouter.get('/', async (req, res) => {
    try {
        if (_.isEmpty(req.query) || !req.query.categoryId) {
            const products = await Product.find({});
            res.json({products});
        } else {
            const category = await Category.findById(req.query.categoryId);
            if (_.isEmpty(category)) {
                return res.status(404).send();
            }
            const products = await Product.find({categoryId: req.query.categoryId});
            res.json({products});
        }
    } catch (e) {
        res.status(400).send(e);
    }
});

productRouter.post('/:categoryId', passport.authenticate('authenticate_jwt', {session: false}), async (req, res) => {
    try {
        const category = await Category.findById(req.params.categoryId);
        if (_.isEmpty(category)) {
            return res.status(404).send();
        }
        const constructor = Object.assign(req.body, {categoryId: req.params.categoryId});
        const product = new Product(constructor);
        const savedProduct = await product.save({validateBeforeSave: true});
        res.json(savedProduct);
    } catch (e) {
        res.status(400).send(e);
    }
});

productRouter.patch('/:_id', passport.authenticate('authenticate_jwt', {session: false}), async (req, res) => {
    try {
        const updatedProduct = await Product.findByIdAndUpdate(req.params._id, req.body, {
            new: true,
            runValidators: true
        });
        if (_.isEmpty(updatedProduct)) {
            return res.status(404).send();
        }
        res.json(updatedProduct);
    } catch (e) {
        res.status(400).send(e);
    }
});

productRouter.delete('/:_id', passport.authenticate('authenticate_jwt', {session: false}), async (req, res) => {
    try {
        const delProduct = await Product.findByIdAndDelete(req.params._id);
        if (_.isEmpty(delProduct)) {
            return res.status(404).send();
        }
        res.json(delProduct);
    } catch (e) {
        res.status(400).send(e);
    }
});
module.exports = productRouter;
