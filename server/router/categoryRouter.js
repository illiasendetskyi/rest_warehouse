const express = require('express');
const _ = require('lodash');
const passport = require('passport');
const {Category} = require('./../models/categories');
const categoryRouter = express.Router();

categoryRouter.get('/', async (req, res) => {
    try {
        const categories = await Category.find({});
        res.json({categories});
    } catch (e) {
        res.status(400).send();
    }
});

categoryRouter.post('/', passport.authenticate('authenticate_jwt', {session: false}), async (req, res) => {
    try {
        const category = new Category(req.body);
        const savedCategory = await category.save({validateBeforeSave: true});
        res.json(savedCategory);
    } catch (e) {
        res.status(400).send(e);
    }
});

categoryRouter.patch('/:_id', passport.authenticate('authenticate_jwt', {session: false}), async (req, res) => {
    try {
        const patchedCategory = await Category.findByIdAndUpdate(req.params._id, req.body, {
            new: true,
            runValidators: true
        });
        if (_.isEmpty(patchedCategory)) {
            return res.status(404).send();
        }
        res.json(patchedCategory);
    } catch (e) {
        res.status(400).send(e);
    }
});

categoryRouter.delete('/:_id', passport.authenticate('authenticate_jwt', {session: false}), async (req, res) => {
    try {
        const delCategory = await Category.findByIdAndDelete(req.params._id);
        if (_.isEmpty(delCategory)) {
            return res.status(404).send();
        }
        res.json(delCategory);
    } catch (e) {
        res.status(400).send(e);
    }
});
module.exports = categoryRouter;
